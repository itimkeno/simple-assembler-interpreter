#include <ctime>
#include <iostream>
#include <unordered_map>
#include <string>
#include <vector>
#include <iterator>

std::unordered_map<std::string, int> assembler(
    const std::vector<std::string> &program)
{
    std::unordered_map<std::string, int> out;

    char reg;
    char cmd;
    std::string value;

    int eax = 0;
    int ebx = 0;
    int ecx = 0;
    int edx = 0;
    int jnz = 0;

    bool eax_flag = false;
    bool ebx_flag = false;
    bool ecx_flag = false;
    bool edx_flag = false;

    for (auto it = program.begin(); it != program.end(); it++)
    {

        reg = (*it)[4];
        cmd = (*it)[0];

        if (reg == 'a')
            eax_flag = true;
        if (reg == 'b')
            ebx_flag = true;
        if (reg == 'c')
            ecx_flag = true;
        if (reg == 'd')
            edx_flag = true;

        if (cmd == 'm')
        {
            value = (*it).substr(6);

            if (reg == 'a')
            {
                if (value != "a" && value != "b" && value != "c" && value != "d")
                {
                    eax = std::stoi(value);
                    continue;
                }
                value == "b" ? eax = ebx : 0;
                value == "c" ? eax = ecx : 0;
                value == "d" ? eax = edx : 0;
                continue;
            }

            if (reg == 'b')
            {
                if (value != "a" && value != "b" && value != "c" && value != "d")
                {
                    ebx = std::stoi(value);
                    continue;
                }
                value == "a" ? ebx = eax : 0;
                value == "c" ? ebx = ecx : 0;
                value == "d" ? ebx = edx : 0;
                continue;
            }
            if (reg == 'c')
            {
                if (value != "a" && value != "b" && value != "c" && value != "d")
                {
                    ecx = std::stoi(value);
                    continue;
                }
                value == "a" ? ecx = eax : 0;
                value == "b" ? ecx = ebx : 0;
                value == "d" ? ecx = edx : 0;
                continue;
            }
            if (reg == 'd')
            {
                if (value != "a" && value != "b" && value != "c" && value != "d")
                {
                    edx = std::stoi(value);
                    continue;
                }
                value == "a" ? edx = eax : 0;
                value == "b" ? edx = ebx : 0;
                value == "c" ? edx = ecx : 0;
                continue;
            }
        }

        if (cmd == 'i')
        {
            if (reg == 'a')
            {
                ++eax;
                continue;
            }
            if (reg == 'b')
            {
                ++ebx;
                continue;
            }
            if (reg == 'c')
            {
                ++ecx;
                continue;
            }
            if (reg == 'd')
            {
                ++edx;
                continue;
            }
        }

        if (cmd == 'd')
        {
            if (reg == 'a')
            {
                --eax;
                continue;
            }
            if (reg == 'b')
            {
                --ebx;
                continue;
            }
            if (reg == 'c')
            {
                --ecx;
                continue;
            }
            if (reg == 'd')
            {
                --edx;
                continue;
            }
        }

        if (cmd == 'j')
        {
            if (reg == 'a')
            {
                if (eax == 0)
                    continue;

                value = (*it).substr(6);

                if (value != "a" && value != "b" && value != "c" && value != "d")
                    jnz = std::stoi(value);

                if (value == "a")
                    jnz = eax;
                if (value == "b")
                    jnz = ebx;
                if (value == "c")
                    jnz = ecx;
                if (value == "d")
                    jnz = edx;

                if (jnz != 0)
                    it += jnz - 1;

                continue;
            }

            if (reg == 'b')
            {
                if (ebx == 0)
                    continue;

                value = (*it).substr(6);

                if (value != "a" && value != "b" && value != "c" && value != "d")
                    jnz = std::stoi(value);

                if (value == "a")
                    jnz = eax;
                if (value == "b")
                    jnz = ebx;
                if (value == "c")
                    jnz = ecx;
                if (value == "d")
                    jnz = edx;

                if (jnz != 0)
                    it += jnz - 1;

                continue;
            }

            if (reg == 'c')
            {
                if (ecx == 0)
                    continue;

                value = (*it).substr(6);

                if (value != "a" && value != "b" && value != "c" && value != "d")
                    jnz = std::stoi(value);

                if (value == "a")
                    jnz = eax;
                if (value == "b")
                    jnz = ebx;
                if (value == "c")
                    jnz = ecx;
                if (value == "d")
                    jnz = edx;

                if (jnz != 0)
                    it += jnz - 1;

                continue;
            }

            if (reg == 'd')
            {
                if (edx == 0)
                    continue;

                value = (*it).substr(6);

                if (value != "a" && value != "b" && value != "c" && value != "d")
                    jnz = std::stoi(value);

                if (value == "a")
                    jnz = eax;
                if (value == "b")
                    jnz = ebx;
                if (value == "c")
                    jnz = ecx;
                if (value == "d")
                    jnz = edx;

                if (jnz != 0)
                    it += jnz - 1;

                continue;
            }
        }
    }

    if (eax_flag)
        out.insert(std::make_pair("a", eax));
    if (ebx_flag)
        out.insert(std::make_pair("b", ebx));
    if (ecx_flag)
        out.insert(std::make_pair("c", ecx));
    if (edx_flag)
        out.insert(std::make_pair("d", edx));

    return out;
}

// std::vector<std::string> program{"mov b 5","mov c 5","mov a 5", "inc a","inc
// b","inc c", "dec a", "dec a",
//       "jnz a -1", "inc a" };
//     std::unordered_map<std::string, int> out{ { "a", 1 } };
//     Assert::That(assembler(program), Equals(out));

int main(int argc, char **argv)
{
    unsigned int start_time = clock();

    std::string str_1 = "";
    std::string str_2 = "";

    std::string test_str = "";
    std::string comp_str = (str_1);

    std::vector<std::string> program{"mov b 2", "mov c 5", "mov a 5",
                                     "mov d a", "mov c 7", "inc a", "inc b",
                                     "inc c", "inc c", "inc c",
                                     "dec a", "dec a", "jnz a -2"};

    std::unordered_map<std::string, int> ans;

    ans = assembler(program);

    for (std::pair<const std::string, int> &i : ans)
        std::cout << i.first << ' ' << i.second << std::endl;

    unsigned int end_time = clock();
    unsigned int time = end_time - start_time;

    std::cout << "total time: " << time << std::endl;

    return std::cout.fail() ? EXIT_FAILURE : EXIT_SUCCESS;
}
